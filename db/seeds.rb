# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Menu.delete_all

Menu.create([{name: 'Ruby burger', description: '1/2 pound with cheese and lettuce',  price: '6'},
             {name: 'Ruby pizza', description: 'Cheese pizza with beef', price: '3'},
             {name: 'Ruby shake', description: 'Mango shake', price: '2'},
             {name: 'Ruby Salad', description: 'Cesar italian style salad', price: '1'}])

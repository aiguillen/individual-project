class CreateCheckouts < ActiveRecord::Migration
  def change
    create_table :checkouts do |t|
      t.integer :ticket_id
      t.boolean :paid_status
      t.float :subtotal
      t.float :tax
      t.float :total

      t.timestamps
    end
  end
end

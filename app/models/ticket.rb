class Ticket < ActiveRecord::Base
  attr_accessible :kitchen_status, :name, :price, :quantity, :subtotal
  belongs_to :menu
  belongs_to :checkout

  validates :name, :price, :quantity,  presence: true

  before_save do
    subtotal
  end
  def subtotal
    self.subtotal = self.quantity * self.price
  end

end

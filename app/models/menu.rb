class Menu < ActiveRecord::Base
  attr_accessible :description, :name, :price

  has_many :tickets
  has_many :checkouts,:through => :tickets

  validates :description, :name, :price, presence: true
end
